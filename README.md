# payment-classification-backend
Так здесь представленнно описание проекта.


**Miro:** https://miro.com/app/board/o9J_lvOEy4w=/
## Структура проекта
Это проект представляет собой микросервисное приложение. 
Соответственно на него налагаются свои структурные паттерны.

### Сервисы

Каждый сервис имеет свою папку, а в каждой папке находятся уже проекты.
* /src
  * /Orchestrator
    * /OrchestratorService
    * /OrchestratorService.UnitTests
    * /OrchestratorService.FunctionalTests
    
Так выше демонстрируются стандарты нейминга проектов. У нас могут существовать несколько типов проектов
| Тип проекта | Нейминг | Описание |
| ---         | ---     | ---      |
| Основной сервис | **ServiceName** | Основной проект сервиса, здесь будет код |
| Инфраструктурный уровень | **ServiceName._Infrastructure_** | Когда наш проект разрастётся, то мы вероятно разделим на эти 3 уровня, но пока это избыточно|
| Доменный уровень | **ServiceName._Domain_** | Когда наш проект разрастётся, то мы вероятно разделим на эти 3 уровня, но пока это избыточно|
| Прикладной уровень| **ServiceName._API_** | Когда наш проект разрастётся, то мы вероятно разделим на эти 3 уровня, но пока это избыточно|
| Юнит тесты | **ServiceName._UnitTests_** | Юнит тесты полезны, их стоит писать, но писать мы будем их писать пост фактум. |
| Интеграционные тесты | **ServiceName._IntegrationTests_** | Эти тесты проверяют как части приложения работают вместе, но интеграционные тесты у нас покрыты функциональными |
| Функциональные тесты | **ServiceName._FunctionalTests_** | Это более общее тестирование по сравнению с интеграционным. Здесь тестируется работа как целого сервиса вместе с зависимостями, так и работа всего приложения. |

#### Orchestrator-service
Этот проект является проектом шаблоном для вас. Задачи орекстратора определны в Miro, и там же его функционал.

При разработке сервисов мы можем частично использовать неоторые общеизвестные паттернаы, такие как DDD, CQRS, ES, но тут как получится по времени.

Сейчас будет разбор некоторых файлов
* `./OrchstratorService` - проект сервиса
    * `./Domain` - соответственно домен сервиса и его логика, не зависящая от языка и инфраструктуры
        * `./Models` - модели
            * `./BaseEntity.cs` - тут определение для DDD entity(domain events), от него наследуются все entity
    * `./Infrstructure` - инфраструктура проекта, все его внешние зависмости и все связанное с ними
        * `./Data` - здесь хранятся как контексты бд, так и конфигурации entity моделей
        * `./Extensions` - extension методы для инетрфейсов и классов
        * `./Repositories` - шаблон generic репозитория, и по необходимости конкретные репозитории
        * `./Services` - собственно наши сервисы, их реализации и интерфейсы
    * `./Migrations` - автогенерированные миграции EF, если не знаете лучше их не трогать
    * `./Application` - прикладной уровень нашего сервиса
        * `./Controllers` - наши контроллеры, логику из них желательно выносить в сервисы, либо по паттерну комманд.
        * `./IntegrationEvents` - интеграционные ивенты - это все ивенты извне, тоесть не доменные. Тут находятся описания ивентов и их обработчики
        * `./EventHandlers` - обработчики событий уровня домена, используются в основном когда нужно выкинуть во внешний мир ивент из домена.
### Прикладные скрипты и деплоймент конфиги

#### migration-helper.ps1 :sparkling_heart: :sparkling_heart: :sparkling_heart:
Моя собственная разработка, мне за нее премию дадут. Это powershell скрипт упрощающий жизнь в больших проектах. Суть в том что ef миграции требуют постоянного указания проекта для миграции, и контекста бд если их несколько.

Но решение есть! :tada: Просто введи в консоль ps данный текст находясь в корне проекта.
> `migration-helper.ps1`

И вас встретит приятный CUI, где вы сможете выбрать проект и контекст с помощью стрелочек, ведь скрипт все спрасил за вас
:heart_eyes: :heart_eyes: :heart_eyes: :heart_eyes: :heart_eyes:  

#### Dockerfile
Внутри основного проекта сервиса декларируется два докерфайла: `Dockerfile` и `Dockerfile.dev`. Первый используется для полноценного персистентного билда, а во втором сконфигурирован .net file watcher, который автоматически подгружает изменнения и перестраивает проекта

#### Docker-compose
Начнем с того, что у нас есть папка `./Docker-compose/dev`. Здесь есть несколько файлов
* `docker-compose.yml` - Compose файл для разработки и тестирования, содержить все необходимы зависимости. Отмечаем, что тут используется `Dockerfile.dev` из проектов, для удобства разработки.
* `docker-compose-tests.yml` - Compose файл для проведения автотестов внутри контейнеров, а также для проведения функциональных тестов в IDE
* `docker-compose-tests.gitlab.yml` - Override для настроек предыдущего файла, который адаптирует его для исполнения на gitlab runner.
* `docker-compose-build.yml` - Compose файл для билда внутри пайплайна

Docker-compose файлы можно запускать прямиком из Rider, и оттуда же очень удобно ими управлять.

#### Gitlab-ci
Тут настроен CI/CD. При коммите в ветки `main` `dev` `staging`, или при mr в них, будут прогонятся тесты. Вы можете изучить конфиги во вкладке `Pipelines` в gitlab, а так-же рассмотрев конфиги в файле `.gitlab-ci.yml` и папке `.gitlab`

