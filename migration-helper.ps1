function DrawMenu {
    ## supportfunction to the Menu function below
    param ($menuItems, $menuPosition, $menuTitel)
    $fcolor = $host.UI.RawUI.ForegroundColor
    $bcolor = $host.UI.RawUI.BackgroundColor
    $l = $menuItems.length
    cls
    $menuwidth = $menuTitel.length + 4
    Write-Host ("*" * $menuwidth) -fore $fcolor -back $bcolor
    Write-Host "* $menuTitel *" -fore $fcolor -back $bcolor
    Write-Host ("*" * $menuwidth) -fore $fcolor -back $bcolor
    Write-Host ""
    Write-debug "L: $l MenuItems: $menuItems MenuPosition: $menuposition"
    for ($i = 0; $i -le $l;$i++) {
        if ($i -eq $menuPosition) {
            Write-Host "$($menuItems[$i])" -fore $bcolor -back $fcolor
        } else {
            Write-Host "$($menuItems[$i])" -fore $fcolor -back $bcolor
        }
    }
}

function Menu {
    param ([array]$menuItems, $menuTitel = "MENU")
    $vkeycode = 0
    $pos = 0
    DrawMenu $menuItems $pos $menuTitel
    While ($vkeycode -ne 13) {
        $press = $host.ui.rawui.readkey("NoEcho,IncludeKeyDown")
        $vkeycode = $press.virtualkeycode
        Write-host "$($press.character)" -NoNewLine
        If ($vkeycode -eq 38) {$pos--}
        If ($vkeycode -eq 40) {$pos++}
        if ($pos -lt 0) {$pos = 0}
        if ($pos -ge $menuItems.length) {$pos = $menuItems.length -1}
        DrawMenu $menuItems $pos $menuTitel
    }
    Write-Output $pos
}

$rawProjects = (dotnet sln list | select -Skip 2).where{$_ -notmatch "(([Uu]nit|[Ff]unctional)[Tt]ests)"}
$projects = [System.Collections.Generic.List[string]]::new();

foreach ($item in $rawProjects)
{
    $projectName = ($item) -replace '/',"\" -split '\',-1,'SimpleMatch'
    $projects.Add($projectName[-1])
}

$selectedProject = $rawProjects[(Menu $projects "Select project:")]

write-host "Building project..."

$rawEfContexts = dotnet ef -p $selectedProject dbcontext list --prefix-output --no-color
$filteredContext = $rawEfContexts.where{$_ -match "^(data:)"} -replace 'data:','' -replace ' ',''
if ($filteredContext.Count -eq 0)
{
    write-host "Something went wrong:"
    foreach ($item in $rawEfContexts)
    {
        write-host $item
    }
    exit;
}

$selectedContext = $rawProjects[(Menu $filteredContext "Select context:")]
$commands = @("add","list","remove");
$command = $commands[(Menu $commands "Select command:")]

$additive = "";
if($command -ne "list"){
    $additive = Read-host "Migration name"
}

dotnet ef -p $selectedProject migrations $command $additive