using System.IO;
using System.Threading.Tasks;

namespace CommonBlocks.Cryptography.Interfaces
{
    /// <summary>
    /// Describes interface for cryptographic signature generation
    /// </summary>
    public interface ISignatureGenerator : ISignatureWorker
    {
        /// <summary>
        ///  Creates signature for given data
        /// </summary>
        /// <param name="data">Stream of data</param>
        /// <param name="skey">Private key for creating signature</param>
        /// <returns>Resulting signature</returns>
        Task<byte[]> CreateSignature(Stream data, byte[] skey);
        
        /// <summary>
        ///  Creates signature for given data
        /// </summary>
        /// <param name="data">Hash data for signing</param>
        /// <param name="skey">Private key for creating signature</param>
        /// <returns>Resulting signature</returns>
        byte[] CreateSignature(byte[] data, byte[] skey);
    }
}