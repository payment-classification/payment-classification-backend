using System.IO;
using System.Threading.Tasks;

namespace CommonBlocks.Cryptography.Interfaces
{
    /// <summary>
    /// Describes methods for validation of cryptographic signatures
    /// </summary>
    public interface ISignatureValidator : ISignatureWorker
    {
        /// <summary>
        /// Verifies signature against given public key
        /// </summary>
        /// <param name="data">Hash for signing</param>
        /// <param name="signature">Signature info</param>
        /// <param name="pkey">Public key for validation of signature</param>
        /// <returns>true if signature valid, false if not</returns>
        bool VerifySignature(byte[] data, byte[] signature, byte[] pkey);
        
        /// <summary>
        /// Verifies signature against given public key
        /// </summary>
        /// <param name="data">Data stream</param>
        /// <param name="signature">Signature info</param>
        /// <param name="pkey">Public key for validation of signature</param>
        /// <returns>true if signature valid, false if not</returns>
        Task<bool> VerifySignature(Stream data, byte[] signature, byte[] pkey);
    }
}