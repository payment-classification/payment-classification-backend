using System;
using System.IO;
using System.Threading.Tasks;
using CommonBlocks.Cryptography.Interfaces;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Security;

namespace CommonBlocks.Cryptography.Implementations
{
    // Я считаю иностранную криптографию более надежной, но bouncycastle поддерживает и российскую
    public class CryptoHasher : ICryptoHasher
    {
        private string _hashAlgorithm;
        private int _maxBlockSize = 1*1024*1024; //  1 MiB
        
        public string HashAlgorithm => _hashAlgorithm;

        public CryptoHasher(string hashAlgorithm)
        {
            _hashAlgorithm = hashAlgorithm;
            
            // Will throw if wrong hash algorithm specified
            DigestUtilities.GetDigest(hashAlgorithm);
        }
        
        public byte[] GenerateHash(byte[] data)
        {
            var sha3Hasher = DigestUtilities.GetDigest(_hashAlgorithm);
            var output = new byte[sha3Hasher.GetDigestSize()];
            
            sha3Hasher.BlockUpdate(data, 0, data.Length);
            sha3Hasher.DoFinal(output, 0);
            
            return output;
        }

        public Task<byte[]> GenerateHashAsync(byte[] data)
        {
            return Task.Run(() => GenerateHash(data));
        }

        public async Task<byte[]> GenerateHashAsync(Stream data)
        {
            var sha3Hasher = DigestUtilities.GetDigest(_hashAlgorithm);
            var output = new byte[sha3Hasher.GetDigestSize()];
            
            if (!data.CanRead) throw new ArgumentException("Stream specified was not readable", nameof(data));
            int readBytes;
            var buffer = new byte[_maxBlockSize];
            
            while ((readBytes = await data.ReadAsync(buffer, 0, buffer.Length)) > 0)
            {
                sha3Hasher.BlockUpdate(buffer,0,readBytes);
            }

            sha3Hasher.DoFinal(output, 0);
            return output;
        }
    }
}