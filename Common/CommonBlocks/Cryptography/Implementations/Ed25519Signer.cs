using System;
using System.IO;
using System.Threading.Tasks;
using CommonBlocks.Cryptography.Interfaces;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace CommonBlocks.Cryptography.Implementations
{
    // Я считаю иностранную криптографию более надежной, но bouncycastle поддерживает и российскую
    public class Ed25519Signer : IFullSigner
    {
        public string SignerAlgorithm { get; } = "Ed25519";
        private readonly ICryptoHasher _cryptoHasher;

        public Ed25519Signer(ICryptoHasher cryptoHasher)
        {
            _cryptoHasher = cryptoHasher ?? throw new ArgumentNullException(nameof(cryptoHasher));
            SignerUtilities.GetSigner(SignerAlgorithm);
        }
        
        public async Task<byte[]> CreateSignature(Stream data, byte[] skey)
        {
            var hash = await _cryptoHasher.GenerateHashAsync(data);
            return CreateSignature(hash, skey);
        }

        public byte[] CreateSignature(byte[] data, byte[] skey)
        {
            var key = new Ed25519PrivateKeyParameters(skey, 0);
            var signer = SignerUtilities.InitSigner(SignerAlgorithm, true, key, SecureRandom.GetInstance("SHA1PRNG"));
            signer.BlockUpdate(data, 0, data.Length);
            return signer.GenerateSignature();
        }

        public bool VerifySignature(byte[] data, byte[] signature, byte[] pkey)
        {
            var key = new Ed25519PublicKeyParameters(pkey, 0);
            var signer = SignerUtilities.InitSigner(SignerAlgorithm, true, key, SecureRandom.GetInstance("SHA1PRNG"));
            signer.BlockUpdate(data, 0, data.Length);
            return signer.VerifySignature(signature);
        }

        public async Task<bool> VerifySignature(Stream data, byte[] signature, byte[] pkey)
        {
            var hash = await _cryptoHasher.GenerateHashAsync(data);
            return VerifySignature(hash, signature, pkey);
        }
    }
}