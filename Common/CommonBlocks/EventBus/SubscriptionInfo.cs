using System;
using EasyNetQ;
using EasyNetQ.Internals;

namespace CommonBlocks.EventBus
{
    public class SubscriptionInfo
    {
        public Type HandlerType { get; }
        
        public AwaitableDisposable<ISubscriptionResult> DisposableSubscriptionResult { get; }

        private SubscriptionInfo(Type handlerType, AwaitableDisposable<ISubscriptionResult> disposableSubscriptionResult)
        {
            HandlerType = handlerType;
            DisposableSubscriptionResult = disposableSubscriptionResult;
        }
        
        public static SubscriptionInfo Typed(Type handlerType, AwaitableDisposable<ISubscriptionResult> disposableSubscriptionResult)
        {
            return new SubscriptionInfo(handlerType, disposableSubscriptionResult);
        }
    }
}