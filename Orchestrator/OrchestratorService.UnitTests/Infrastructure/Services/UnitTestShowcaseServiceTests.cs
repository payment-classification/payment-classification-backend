using System;
using Xunit;
using OrchestratorService.Infrastructure.Services;

namespace OrchestratorService.Tests.Infrastructure.Services
{
    // В юнит тестах мы можем создавать моки внешних зависимостей, например зачем нам настоязая бд когда можно ее мокнуть
    public class UnitTestShowcaseServiceTests
    {
        [Fact]
        public void ShouldArgumentExceptionThrowWhen1()
        {
            var obj = new UnitTestShowcaseService();
            Assert.Throws<ArgumentException>(() => obj.ShouldArgumentExceptionThrowWhen1(1));
        }

        [Fact]
        public void ShouldNotThrowWhenNot1()
        {
            var obj = new UnitTestShowcaseService();
            obj.ShouldArgumentExceptionThrowWhen1(5);
            obj.ShouldArgumentExceptionThrowWhen1(-15);
        }

        [Theory]
        [InlineData(1, 2, 2)]
        [InlineData(2, 1, 2)]
        [InlineData(2, 2, 4)]
        [InlineData(2, -2, -4)]
        [InlineData(-2, 2, -4)]
        [InlineData(-2, -2, 4)]
        [InlineData(-2, 0, 0)]
        public void MultiplyShouldCalculateCorrectly(int a, int b, int result)
        {
            var obj = new UnitTestShowcaseService();
            Assert.Equal(obj.Multiply(a,b), result);
        }
    }
}