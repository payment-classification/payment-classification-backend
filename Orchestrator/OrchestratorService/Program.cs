using System;
using System.IO;
using CommonBlocks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using OrchestratorService.Infrastructure.Data;
using Serilog;

namespace OrchestratorService
{
    public class Program
    {
        public static string Namespace = typeof(Startup).Namespace;

        public static string AppName = Namespace.IndexOf(".", StringComparison.Ordinal) != -1
            ? Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1)
            : Namespace;

        public static void Main(string[] args)
        {
            try
            {
                var configuration = GetConfiguration();
                Log.Logger = CreateSerilogLogger(configuration);
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);

                var webhost = BuildWebHost(args);

                Log.Information("Applying migrations ({ApplicationContext})...", AppName);

                webhost.MigrateDbContext<OrchestratorDbContext>((context, services) =>
                {
                    context.Database.EnsureCreated();
                });
                
                Log.Information("Starting webhost ({ApplicationContext})...", AppName);

                webhost.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration(x => x.AddConfiguration(GetConfiguration()))
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseSerilog()
                .Build();
        
        static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            
            return builder.Build();
        }
        
        static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            var seqServerUrl = configuration["SeqServerUrl"];
            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
    }
}