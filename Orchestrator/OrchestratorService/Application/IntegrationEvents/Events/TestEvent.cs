using CommonBlocks.EventBus;

namespace OrchestratorService.Application.IntegrationEvents.Events
{
    public record TestEvent : IntegrationEvent
    {
        public string TestString { get; init; }
    }
}