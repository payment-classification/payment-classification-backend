using System.Net;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.AspNetCore.Mvc;
using OrchestratorService.Application.IntegrationEvents.Events;

namespace OrchestratorService.Application.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TestShowcaseController : ControllerBase
    {
        private readonly IBus _bus;

        public TestShowcaseController(IBus bus)
        {
            _bus = bus;
        }

        [HttpGet]
        public ActionResult TestPassword([FromQuery] string password)
        {
            return password == "correct" ? Ok("secret") : StatusCode((int) HttpStatusCode.Forbidden, $"{password}");
        }

        [HttpGet("RabbitMQ")]
        public async Task<OkResult> TestRabbitMq([FromQuery] string msg)
        {
            await _bus.PubSub.PublishAsync(new TestEvent() {TestString = msg});
            return Ok();
        }
    }
}