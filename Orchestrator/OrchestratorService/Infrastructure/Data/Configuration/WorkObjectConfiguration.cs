using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrchestratorService.Domain.Models;

namespace OrchestratorService.Infrastructure.Data.Configuration
{
    public class WorkObjectConfiguration : IEntityTypeConfiguration<WorkObject>
    {
        public void Configure(EntityTypeBuilder<WorkObject> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Ignore(b => b.DomainEvents);
            builder.Ignore(b => b.CurrentStatus);

            builder.Property(b => b.Id)
                .UseHiLo("workobjectSeq", OrchestratorDbContext.DEFAULT_SCHEMA);

            builder.HasMany(x => x.ProcessingStatusHistory)
                .WithOne()
                .HasForeignKey(x => x.WorkObjectId);

            var navigation = builder.Metadata.FindNavigation(nameof(WorkObject.ProcessingStatusHistory));
            navigation.SetField("_processingStatusHistory");
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}