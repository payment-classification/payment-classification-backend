using System;

namespace OrchestratorService.Infrastructure.Services
{
    /*
     * Данный класс является демонстрацией принципов юнит тестирования,
     * Посмотрите на тесты, и попробуйте слломать 
     */
    public class UnitTestShowcaseService
    {
        public void ShouldArgumentExceptionThrowWhen1(int input)
        {
            if (input == 1) throw new ArgumentException(nameof(input));
        }

        public int Multiply(int a, int b)
        {
            //Попробуй сломать тут и запустить тесты
            return a * b;
        }
    }
}