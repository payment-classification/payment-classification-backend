using System.Collections.Generic;
using System.Linq;

namespace OrchestratorService.Domain.Models
{
  
    public class WorkObject : BaseEntity, IAggregateRoot
    {
        public ProcessingStatusSnapshot CurrentStatus => _processingStatusHistory.Last();

        private readonly List<ProcessingStatusSnapshot> _processingStatusHistory = new List<ProcessingStatusSnapshot>();
        
        public IReadOnlyCollection<ProcessingStatusSnapshot> ProcessingStatusHistory => _processingStatusHistory.AsReadOnly();
        
        public string FileName { get; init; }
        
        //ID User создавшего задачу
        public int CreatedById { get; init; }

        public void UpdateStatus()
        {
            
        }
    }
}