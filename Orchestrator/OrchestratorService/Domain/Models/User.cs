namespace OrchestratorService.Domain.Models
{
    public class User : BaseEntity, IAggregateRoot
    {
        public byte[] PublicEcdsaKey { get; set; }
    }
}